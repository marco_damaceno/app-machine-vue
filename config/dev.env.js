var merge = require('webpack-merge')
var prodEnv = require('./prod.env')
var env = require('./.env')

module.exports = merge(prodEnv, env, {
  NODE_ENV: '"development"'
})
