# App Machine Vue

App created by Marco Damaceno for App Machine`s selective process. It lists Youtube channels for developers. It is made up with **Vue.js**, **Firebase** and **Bootstrap**.

Credentials for setting up Firebase will be sent by email. Create a file named **.env.js** and place it in **config** folder with the following content:

``` javascript
module.exports = {
  FIREBASE_API_KEY: '"%%%"',
  FIREBASE_AUTH_DOMAIN: '"%%%"',
  FIREBASE_DB_URL: '"%%%"',
  FIREBASE_PROJECT_ID: '"%%%"',
  FIREBASE_STORAGE_BUCKET: '"%%%"',
  FIREBASE_MESSAGING_SENDER_ID: '"%%%"'
}
```

Change the values %%% with the ones sent by email.

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```

For detailed explanation on how things work, checkout the [guide](http://vuejs-templates.github.io/webpack/) and [docs for vue-loader](http://vuejs.github.io/vue-loader).
